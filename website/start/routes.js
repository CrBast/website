'use strict';

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');

Route.any('/', ({ response, session, view }) => {
  let showNotif = false;
  if (!session.get('showNotif')) {
    session.put('showNotif', 'false');
    showNotif = true;
  }
  return view.render('welcome', { showNotif: showNotif });
}).as('home');

Route.group(() => {
  Route.get('news', 'api/NewsController.show');
  Route.resource('news', 'api/NewsController')
    .only(['store', 'destroy'])
    .middleware(['authAdmin']);
  Route.resource('users', 'api/UserController')
    .except(['index', 'show'])
    .middleware(['authAdmin']);
  Route.resource('persons', 'api/PersonController')
    .except(['index', 'show'])
    .middleware(['authAdmin']);
  Route.resource('projects', 'api/ProjectController')
    .except(['index', 'show'])
    .middleware(['authAdmin']);

  Route.resource('companies', 'api/CompanyController')
    .only(['store'])
    .middleware(['authVerif']);
  Route.resource('companies', 'api/CompanyController')
    .only(['update', 'destroy'])
    .middleware(['authAdminOrOwnerGuest:company']);

  Route.resource('job-offers', 'api/JobOfferController')
    .only(['store'])
    .middleware(['authVerif']);
  Route.resource('job-offers', 'api/JobOfferController')
    .only(['update', 'destroy'])
    .middleware(['authAdminOrOwnerGuest:job_offer']);

  Route.resource('users', 'api/UserController')
    .only(['index', 'show'])
    .middleware(['authVerif']);
  Route.resource('persons', 'api/PersonController').only(['index', 'show']);
  Route.resource('projects', 'api/ProjectController').only(['index', 'show']);
  Route.resource('companies', 'api/CompanyController').only(['index', 'show']);
  Route.resource('job-offers', 'api/JobOfferController').only([
    'index',
    'show'
  ]);

  Route.post('auth', 'api/AuthController.login');
  Route.post('auth/logout', 'api/AuthController.logout');

  Route.any('*', ({ response }) =>
    response.status(404).send({
      message: 'route not found',
      help: 'https://gitlab.com/CrBast/website/wikis/home'
    })
  );
})
  .prefix('api')
  .formats(['json']);

Route.any('*', ({ response }) => response.route('home'));
