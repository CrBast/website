const { hooks } = use('@adonisjs/ignitor');

hooks.after.providersBooted(() => {
  const Env = use('Env');
  const View = use('View');

  View.global('ANALYTICS_HOST', function() {
    return Env.get('ANALYTICS_HOST');
  });
  View.global('ANALYTICS_KEY', function() {
    return Env.get('ANALYTICS_KEY');
  });
});
