# Adonis fullstack application

This is the fullstack boilerplate for AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Session
3. Authentication
4. Web security middleware
5. CORS
6. Edge template engine
7. Lucid ORM
8. Migrations and seeds

## Setup

Use the adonis command to install dependencies.

```bash
npm install
```


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```

Run the following command to create default api user.
```js
adonis seed
```
***Credentials :***
- *Email : hello@crbast.ch*
- *Password : 123456*
