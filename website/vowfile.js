'use strict';

/*
|--------------------------------------------------------------------------
| Vow file
|--------------------------------------------------------------------------
|
| The vow file is loaded before running your tests. This is the best place
| to hook operations `before` and `after` running the tests.
|
*/

// Uncomment when want to run migrations
const ace = require('@adonisjs/ace');

module.exports = (cli, runner) => {
  runner.before(async () => {
    /*
    |--------------------------------------------------------------------------
    | Start the server
    |--------------------------------------------------------------------------
    |
    | Starts the http server before running the tests. You can comment this
    | line, if http server is not required
    |
    */
    use('Adonis/Src/Server').listen(process.env.HOST, process.env.PORT);

    /*
    |--------------------------------------------------------------------------
    | Run migrations
    |--------------------------------------------------------------------------
    |
    | Migrate the database before starting the tests.
    |
    */
    await ace.call('migration:refresh', {}, { silent: true });

    const Hash = use('Hash');
    const Database = use('Database');

    await Database.table('users').insert({
      email: 'hello@crbast.ch',
      role: 'admin',
      password: await Hash.make('123456'),
      created_at: Database.fn.now(),
      updated_at: Database.fn.now()
    });
    const guest1 = await Database.table('users').insert({
      email: 'guest1@crbast.ch',
      role: 'guest',
      password: await Hash.make('guest1'),
      created_at: Database.fn.now(),
      updated_at: Database.fn.now()
    });
    const guest2 = await Database.table('users').insert({
      email: 'guest2@crbast.ch',
      role: 'guest',
      password: await Hash.make('guest2'),
      created_at: Database.fn.now(),
      updated_at: Database.fn.now()
    });
    await Database.table('people').insert({
      firstname: 'Bob',
      lastname: 'Boby',
      email: 'hello@bo.by',
      location: 'Switzerland',
      skils: { adonis: 'love', node: 'good' },
      created_at: Database.fn.now(),
      updated_at: Database.fn.now()
    });
    const neptiumCompany = await Database.table('companies').insert({
      name: 'Neptium',
      user_id: guest1[0],
      description:
        'Neptium is a young digital company specialized in the development of custom applications for web, multi-platform software and other applications.',
      created_at: Database.fn.now(),
      updated_at: Database.fn.now()
    });
    await Database.table('companies').insert({
      name: 'JetBrains',
      user_id: guest2[0],
      description:
        'JetBrains s.r.o. (formerly IntelliJ Software s.r.o.) is a software development company whose tools are targeted towards software developers and project managers.',
      created_at: Database.fn.now(),
      updated_at: Database.fn.now()
    });
    await Database.table('job_offers').insert({
      company_id: neptiumCompany[0],
      user_id: guest1[0],
      details: 'Love',
      url: 'www.neptium.ch',
      contact: 'hello.test@neptium.ch',
      isSerious: true,
      created_at: Database.fn.now(),
      updated_at: Database.fn.now()
    });
  });

  runner.after(async () => {
    /*
    |--------------------------------------------------------------------------
    | Shutdown server
    |--------------------------------------------------------------------------
    |
    | Shutdown the HTTP server when all tests have been executed.
    |
    */
    use('Adonis/Src/Server')
      .getInstance()
      .close();

    /*
    |--------------------------------------------------------------------------
    | Rollback migrations
    |--------------------------------------------------------------------------
    |
    | Once all tests have been completed, we should reset the database to it's
    | original state
    |
    */
    // await ace.call('migration:reset', {}, { silent: true })
  });
};
