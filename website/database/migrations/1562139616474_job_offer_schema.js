'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class JobOfferSchema extends Schema {
  up() {
    this.create('job_offers', table => {
      table.increments();
      table
        .integer('company_id')
        .unsigned()
        .references('id')
        .inTable('companies');
      table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users'); // Created by user_id
      table.string('details', 255).notNullable();
      table.string('url', 200).nullable();
      table.string('contact', 200).nullable();
      table.boolean('isSerious').notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop('job_offers');
  }
}

module.exports = JobOfferSchema;
