'use strict';

const { test, trait } = use('Test/Suite')('Companies');

const Company = use('App/Models/Company');
const User = use('App/Models/User');

trait('Test/ApiClient');
trait('Auth/Client');
trait('Session/Client');

test('Companies get list', async ({ client, assert }) => {
  const response = await client.get('api/companies').end();
  response.assertStatus(200);
  assert.isNotNull(response.body);
  var company = response.body[0];
  const dbCompany = await Company.findOrFail(company.id);
  assert.equal(company.name, dbCompany.name);
  assert.equal(company.description, dbCompany.description);
});

test('Companies get specific company', async ({ client, assert }) => {
  const response = await client.get('api/companies/1').end();
  response.assertStatus(200);
  var company = response.body;
  assert.isNotNull(company);
  const dbCompany = await Company.find(company.id);
  assert.equal(company.firstname, dbCompany.firstname);
  assert.equal(company.lastname, dbCompany.lastname);
  assert.equal(company.email, dbCompany.email);
});

test('Companies add new company', async ({ client, assert }) => {
  const user = await User.findBy('email', 'guest1@crbast.ch');
  var company = {
    name: 'SpaceX',
    description: 'Eyes in the stars.'
  };
  const response = await client
    .post('api/companies')
    .loginVia(user, 'jwt')
    .query(company)
    .end();
  response.assertStatus(200);
  var company2 = response.body;
  assert.isNotNull(company2.id);
  assert.equal(company2.name, 'SpaceX');
  assert.equal(company2.description, 'Eyes in the stars.');
  const companyToDelete = await Company.findOrFail(company2.id);
  companyToDelete.delete();
});

test('Companies modify company', async ({ client, assert }) => {
  const user = await User.findBy('email', 'guest1@crbast.ch');
  var company = {
    name: 'SpaceX',
    description: 'Eyes in the stars.'
  };
  var response = await client
    .post('api/companies')
    .loginVia(user, 'jwt')
    .query(company)
    .end();
  response.assertStatus(200);
  var company2 = response.body;
  assert.isNotNull(company2.id);
  assert.equal(company2.name, 'SpaceX');
  assert.equal(company2.description, 'Eyes in the stars.');

  const dbCompany = await Company.findOrFail(company2.id);
  var newCompany = {
    description: 'A Testla in space'
  };
  var response2 = await client
    .put(`api/companies/${dbCompany.id}`)
    .loginVia(user, 'jwt')
    .query(newCompany)
    .end();
  response2.assertStatus(200);
  var newCompany2 = response2.body;
  assert.isNotNull(newCompany2.id);
  assert.equal(newCompany2.name, dbCompany.name);
  assert.equal(newCompany2.description, 'A Testla in space');
  dbCompany.delete();
});

test('Companies delete company', async ({ client, assert }) => {
  const user = await User.findBy('email', 'guest1@crbast.ch');
  var company = {
    name: 'SpaceX',
    description: 'Eyes in the stars.'
  };
  var response = await client
    .post('api/companies')
    .loginVia(user, 'jwt')
    .query(company)
    .end();
  response.assertStatus(200);
  var company2 = response.body;
  assert.isNotNull(company2.id);
  assert.equal(company2.name, 'SpaceX');
  assert.equal(company2.description, 'Eyes in the stars.');

  const dbCompany = await Company.findOrFail(company2.id);
  var response2 = await client
    .delete(`api/companies/${dbCompany.id}`)
    .loginVia(user, 'jwt')
    .end();
  response2.assertStatus(200);
  var emptyCompany = await Company.find(dbCompany.id);
  assert.isNull(emptyCompany, 'Company are not deleted');
  var response3 = await client.get(`api/companies/${dbCompany.id}`).end();
  response3.assertStatus(404);
});

test('Companies unauthorized modification', async ({ client, assert }) => {
  const user = await User.findBy('email', 'guest1@crbast.ch');
  var company = {
    name: 'Google',
    description: 'We want your privacy.'
  };
  var response = await client
    .post('api/companies')
    .loginVia(user, 'jwt')
    .query(company)
    .end();
  response.assertStatus(200);
  var company2 = response.body;
  assert.isNotNull(company2.id);
  assert.equal(company2.name, 'Google');
  assert.equal(company2.description, 'We want your privacy.');

  const guest2 = await User.findBy('email', 'guest2@crbast.ch');
  var response2 = await client
    .put(`api/companies/${company2.id}`)
    .loginVia(guest2, 'jwt')
    .end();
  response2.assertStatus(401);
  const dbCompany = await Company.findOrFail(company2.id);
  dbCompany.delete();
});

test('Companies admin modification guest entry', async ({ client, assert }) => {
  const user = await User.findBy('email', 'guest1@crbast.ch');
  var company = {
    name: 'Google',
    description: 'We want your privacy.'
  };
  var response = await client
    .post('api/companies')
    .loginVia(user, 'jwt')
    .query(company)
    .end();
  response.assertStatus(200);
  var company2 = response.body;
  assert.isNotNull(company2.id);
  assert.equal(company2.name, 'Google');
  assert.equal(company2.description, 'We want your privacy.');

  const admin = await User.findBy('email', 'hello@crbast.ch');
  var response2 = await client
    .put(`api/companies/${company2.id}`)
    .loginVia(admin, 'jwt')
    .end();
  response2.assertStatus(200);
  const dbCompany = await Company.findOrFail(company2.id);
  dbCompany.delete();
});
