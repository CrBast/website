'use strict';

const { test, trait } = use('Test/Suite')('Auth');

trait('Test/ApiClient');

test('Auth default admin account', async ({ client, assert }) => {
  const response = await client
    .post('api/auth')
    .query({ email: 'hello@crbast.ch', password: '123456' })
    .end();
  response.assertStatus(200);
  var body = response.body;
  assert.isDefined(body.type, 'Type is null');
  assert.isDefined(body.token, 'Token is null');
  assert.isDefined(body.refreshToken, 'RefreshToken is null');
});

test('Auth guest account', async ({ client, assert }) => {
  const response = await client
    .post('api/auth')
    .query({ email: 'guest1@crbast.ch', password: 'guest1' })
    .end();
  response.assertStatus(200);
  var body = response.body;
  assert.isDefined(body.type, 'Type is null');
  assert.isDefined(body.token, 'Token is null');
  assert.isDefined(body.refreshToken, 'RefreshToken is null');
});

test('Auth bad login', async ({ client, assert }) => {
  const response = await client
    .post('api/auth')
    .query({ email: 'hello@crbast.chh', password: 'hhhhhh' })
    .end();
  response.assertStatus(401);
});
