'use strict';

const { test, trait } = use('Test/Suite')('Job Offers');

const Company = use('App/Models/Company');
const JobOffer = use('App/Models/JobOffer');
const User = use('App/Models/User');

trait('Test/ApiClient');
trait('Auth/Client');
trait('Session/Client');

test('Job offers get list', async ({ client, assert }) => {
  const response = await client.get('api/job-offers').end();
  response.assertStatus(200);
  assert.isNotNull(response.body);
  var jobOffer = response.body[0];
  const dbJobOffer = await JobOffer.findOrFail(jobOffer.id);
  assert.equal(jobOffer.name, dbJobOffer.name);
  assert.equal(jobOffer.description, dbJobOffer.description);
});

test('Job offers get specific offer', async ({ client, assert }) => {
  const response = await client.get('api/job-offers/1').end();
  response.assertStatus(200);
  var jobOffer = response.body;
  assert.isNotNull(jobOffer);
  const dbJobOffer = await JobOffer.find(jobOffer.id);
  assert.equal(jobOffer.details, dbJobOffer.details);
  assert.equal(jobOffer.url, dbJobOffer.url);
  assert.equal(jobOffer.contact, undefined); // Hidden field
});

test('Job offers add new offer', async ({ client, assert }) => {
  const user = await User.findBy('email', 'guest1@crbast.ch');
  const neptiumCompany = await Company.findBy('name', 'Neptium');
  var jobOffer = {
    company_id: neptiumCompany.id,
    details: 'Big kiss',
    url: 'www.neptium.ch',
    contact: 'hello.test@neptium.ch',
    isSerious: true
  };
  const response = await client
    .post('api/job-offers')
    .loginVia(user, 'jwt')
    .query(jobOffer)
    .end();
  response.assertStatus(200);
  var jobOffer2 = response.body;
  assert.isNotNull(jobOffer2.id);
  assert.equal(jobOffer2.company_id, neptiumCompany.id);
  assert.equal(jobOffer2.details, 'Big kiss');
  assert.equal(jobOffer2.url, 'www.neptium.ch');
  const jobOfferToDelete = await JobOffer.findOrFail(jobOffer2.id);
  jobOfferToDelete.delete();
});

test('Job offers add new offer with unauthorized company assignment', async ({
  client,
  assert
}) => {
  const user = await User.findBy('email', 'guest1@crbast.ch');
  const jetBrainsCompany = await Company.findBy('name', 'JetBrains');
  var jobOffer = {
    company_id: jetBrainsCompany.id,
    details: 'Hi, ...',
    url: 'https://www.jetbrains.com/careers/jobs/',
    contact: 'jobs@jetbrains.com',
    isSerious: true
  };
  const response = await client
    .post('api/job-offers')
    .loginVia(user, 'jwt')
    .query(jobOffer)
    .end();
  response.assertStatus(401);
  assert.isNotNull(response.body);
});

test('Job offers modify offer', async ({ client, assert }) => {
  const user = await User.findBy('email', 'guest1@crbast.ch');
  const neptiumJobOffer = await JobOffer.query()
    .where('user_id', user.id)
    .first();

  var newJobOffer = {
    details: 'Hi boyyyy!',
    isSerious: false
  };
  var response = await client
    .put(`api/job-offers/${neptiumJobOffer.id}`)
    .loginVia(user, 'jwt')
    .query(newJobOffer)
    .end();
  response.assertStatus(200);
  var newJobOffer2 = response.body;

  const dbJobOffer = await JobOffer.findOrFail(newJobOffer2.id);
  assert.isNotNull(newJobOffer2.id);
  assert.equal(newJobOffer2.details, 'Hi boyyyy!');
  assert.equal(newJobOffer2.company_id, dbJobOffer.id);
  assert.equal(newJobOffer2.url, 'www.neptium.ch');
  assert.isFalse(newJobOffer2.isSerious, 'Bad job offer <isSerious> value');
});

test('Job offers modify offer with unauthorized company assignment', async ({
  client,
  assert
}) => {
  const user = await User.findBy('email', 'guest1@crbast.ch');
  const neptiumJobOffer = await JobOffer.query()
    .where('user_id', user.id)
    .first();
  const jetBrainsCompany = await Company.findBy('name', 'JetBrains');

  var newJobOffer = {
    details: 'Hi boy!',
    company_id: jetBrainsCompany.id,
    isSerious: false
  };

  var response = await client
    .put(`api/job-offers/${neptiumJobOffer.id}`)
    .loginVia(user, 'jwt')
    .query(newJobOffer)
    .end();
  response.assertStatus(401);
});

test('Job offers modify offer with unauthorized user', async ({
  client,
  assert
}) => {
  const guest1 = await User.findBy('email', 'guest1@crbast.ch');
  const guest2 = await User.findBy('email', 'guest2@crbast.ch');
  const neptiumJobOffer = await JobOffer.query()
    .where('user_id', guest1.id)
    .first();

  var newJobOffer = {
    details: 'Hi boy!',
    isSerious: false
  };
  var response = await client
    .put(`api/job-offers/${neptiumJobOffer.id}`)
    .loginVia(guest2, 'jwt')
    .query(newJobOffer)
    .end();
  response.assertStatus(401);
});

test('Job offers modify offer with admin', async ({ client, assert }) => {
  const guest1 = await User.findBy('email', 'guest1@crbast.ch');
  const admin = await User.findBy('email', 'hello@crbast.ch');
  const neptiumJobOffer = await JobOffer.query()
    .where('user_id', guest1.id)
    .first();

  var newJobOffer = {
    isSerious: true
  };
  var response = await client
    .put(`api/job-offers/${neptiumJobOffer.id}`)
    .loginVia(admin, 'jwt')
    .query(newJobOffer)
    .end();
  response.assertStatus(200);
  var offerAnswer = response.body;
  assert.isTrue(offerAnswer.isSerious, 'Bad job offer <isSerious> value');
});

test('Job offers delete offer', async ({ client, assert }) => {
  const guest1 = await User.findBy('email', 'guest1@crbast.ch');
  const neptiumCompany = await Company.findBy('name', 'Neptium');

  var jobOffer = {
    company_id: neptiumCompany.id,
    details: 'to delete',
    url: 'www.neptium.ch',
    contact: 'hello.test@neptium.ch',
    isSerious: false
  };

  var response = await client
    .post('api/job-offers')
    .loginVia(guest1, 'jwt')
    .query(jobOffer)
    .end();
  response.assertStatus(200);

  var offerId = response.body.id;
  var response2 = await client
    .delete(`api/job-offers/${offerId}`)
    .loginVia(guest1, 'jwt')
    .end();
  response2.assertStatus(200);
  assert.isNull(await JobOffer.find(offerId));
});

test('Job offers delete offer with unauthorized user', async ({ client }) => {
  const guest1 = await User.findBy('email', 'guest1@crbast.ch');
  const guest2 = await User.findBy('email', 'guest2@crbast.ch');
  const neptiumCompany = await Company.findBy('name', 'Neptium');

  var jobOffer = {
    company_id: neptiumCompany.id,
    details: 'to delete',
    url: 'www.neptium.ch',
    contact: 'hello.test@neptium.ch',
    isSerious: false
  };

  var response = await client
    .post('api/job-offers')
    .loginVia(guest1, 'jwt')
    .query(jobOffer)
    .end();
  response.assertStatus(200);

  var offerId = response.body.id;
  var response2 = await client
    .delete(`api/job-offers/${offerId}`)
    .loginVia(guest2, 'jwt')
    .end();
  response2.assertStatus(401);

  const offerToDelete = await JobOffer.findOrFail(offerId);
  offerToDelete.delete();
});

test('Job offers delete offer with unauthorized user', async ({ client }) => {
  const guest1 = await User.findBy('email', 'guest1@crbast.ch');
  const admin = await User.findBy('email', 'hello@crbast.ch');
  const neptiumCompany = await Company.findBy('name', 'Neptium');

  var jobOffer = {
    company_id: neptiumCompany.id,
    details: 'to delete',
    url: 'www.neptium.ch',
    contact: 'hello.test@neptium.ch',
    isSerious: false
  };

  var response = await client
    .post('api/job-offers')
    .loginVia(guest1, 'jwt')
    .query(jobOffer)
    .end();
  response.assertStatus(200);

  var offerId = response.body.id;
  var response2 = await client
    .delete(`api/job-offers/${offerId}`)
    .loginVia(admin, 'jwt')
    .end();
  response2.assertStatus(200);
});
