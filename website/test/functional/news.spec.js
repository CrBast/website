'use strict';

const { test, trait } = use('Test/Suite')('News');

const News = use('App/Models/News');
const User = use('App/Models/User');

trait('Test/ApiClient');
trait('Auth/Client');
trait('Session/Client');

test('News get news', async ({ client, assert }) => {
  const user = await User.findBy('email', 'hello@crbast.ch');
  var news = {
    title: 'Test1',
    details: 'Test1',
    contact: 'test1@test.com'
  };
  var response = await client
    .post('api/news')
    .loginVia(user, 'jwt')
    .query(news)
    .end();
  response.assertStatus(200);
  var response2 = await client.get('api/news').end();
  response2.assertStatus(200);
  var news2 = response2.body;
  assert.isNotNull(news2.id);
  assert.equal(news2.title, 'Test1');
  assert.equal(news2.details, 'Test1');
  assert.equal(news2.contact, 'test1@test.com');
  assert.isNull(news2.url);
});

test('News add news', async ({ client, assert }) => {
  const user = await User.findBy('email', 'hello@crbast.ch');
  var news = {
    title: 'Test1',
    details: 'Test1',
    contact: 'test1@test.com'
  };
  const response = await client
    .post('api/news')
    .loginVia(user, 'jwt')
    .query(news)
    .end();
  response.assertStatus(200);
  var news2 = response.body;
  assert.isNotNull(news2.id);
  assert.equal(news2.title, 'Test1');
  assert.equal(news2.details, 'Test1');
  assert.equal(news2.contact, 'test1@test.com');
  const newsToDelete = await News.findOrFail(news2.id);
  newsToDelete.delete();
});

test('News delete news', async ({ client, assert }) => {
  const user = await User.findBy('email', 'hello@crbast.ch');
  var news = {
    title: 'Test1',
    details: 'Test1',
    contact: 'test1@test.com'
  };
  var response = await client
    .post('api/news')
    .loginVia(user, 'jwt')
    .query(news)
    .end();
  response.assertStatus(200);
  var id = response.body.id;
  var response2 = await client
    .delete(`api/news/${id}`)
    .loginVia(user, 'jwt')
    .end();
  response2.assertStatus(200);
  assert.isNull(await News.find(id));
});
