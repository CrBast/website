'use strict';
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Company = use('App/Models/Company');
const JobOffer = use('App/Models/JobOffer');

// TODO add unit test for this middleware

class AuthAdminOrOwnerGuest {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ request, auth, response, params }, next, properties) {
    // call next to advance the request
    try {
      await auth.check();
      if (auth.user.role !== 'admin') {
        var userId = null;

        switch (properties[0]) {
          case 'company':
            var company = await Company.find(params.id);
            userId = company.user_id;
            break;
          case 'job_offer':
            var jobOffer = await JobOffer.find(params.id);
            userId = jobOffer.user_id;
            break;
          default:
            throw new Error(
              `Middleware [AuthAdminOrOwnerGuest] parameter : <${properties[0]}> not implemented`
            );
        }

        if (auth.user.id !== userId) {
          throw new Error('Only the creator of the entry can modify it.');
        }
      }
    } catch (error) {
      return response.status(401).send({ message: error.message });
    }
    await next();
  }
}

module.exports = AuthAdminOrOwnerGuest;
