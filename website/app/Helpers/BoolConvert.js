'use strict';

/**
 * Convert boolean db value to JS boolean
 *
 * @param {*} value
 * @return {Boolean} Convert result
 */
const boolConverter = value => {
  switch (typeof value) {
    case 'number':
      switch (value) {
        case 1:
          return true;
        case 0:
          return false;
        default:
          return null;
      }
    case 'string':
      switch (value) {
        case 'true':
          return true;
        case 'false':
          return false;
        default:
          return null;
      }
    case 'boolean':
      return value;
    default:
      return null;
  }
};

module.exports = {
  boolConverter
};
