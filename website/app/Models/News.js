'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

/**
 * @swagger
 * definitions:
 *  News:
 *      type: object
 *      properties:
 *          id:
 *              type: integer
 *          title:
 *              type: string
 *          details:
 *              type: string
 *          contact:
 *              type: string
 *          url:
 *              type: string
 *      required:
 *          - title
 *          - details
 */
class News extends Model {}

module.exports = News;
