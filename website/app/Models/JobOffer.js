'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

/**
 * @swagger
 * definitions:
 *  JobOffer:
 *      type: object
 *      properties:
 *          id:
 *              type: integer
 *          company_id:
 *              type: integer
 *              info: It is only possible to create the instance if the specified company was created by the same user.
 *          user_id:
 *              type: integer
 *              info: This field cannot be modified by api. It is automatically applied with the id of the user who created it.
 *          details:
 *              type: string
 *          url:
 *              type: string
 *          contact:
 *              type: string
 *              hidden: true
 *          isSerious:
 *              type: boolean
 *      required:
 *          - company_id
 *          - user_id
 *          - details
 *          - isSerious
 */
class JobOffer extends Model {
  static get hidden() {
    return ['contact'];
  }

  company() {
    return this.belongsTo('App/Models/Company');
  }

  user() {
    return this.belongsTo('App/Models/User');
  }
}

module.exports = JobOffer;
