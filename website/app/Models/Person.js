'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

/**
 * @swagger
 * definitions:
 *  Person:
 *      type: object
 *      properties:
 *          id:
 *              type: integer
 *          firstname:
 *              type: string
 *          lastname:
 *              type: string
 *          email:
 *              type: string
 *          location:
 *              type: string
 *          skils:
 *              type: json
 *              json: {"NodeJS":"good","PHP": "good"}
 *      required:
 *          - firstname
 *          - lastname
 *          - email
 *          - location
 */
class Person extends Model {
  jobHistories() {
    return this.hasMany('App/Models/JobHistory');
  }

  companies() {
    return this.belongsToMany('App/Models/Company').pivotModel(
      'App/Models/JobHistory'
    );
  }

  projects() {
    return this.hasMany('App/Models/Project');
  }
}

module.exports = Person;
