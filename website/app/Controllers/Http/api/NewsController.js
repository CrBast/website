'use strict';
const News = use('App/Models/News');

class NewsController {
  /**
   * @swagger
   *
   * /news:
   *  get:
   *      tags:
   *          - News
   *      summary: Get latest news
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                    $ref: "#/definitions/News"
   *          404:
   *              description: Not found
   */
  async show({ response }) {
    var latestNews = await News.last();
    if (latestNews == null)
      return response.status(404).send({ message: 'There is no news' });
    return latestNews;
  }

  /**
   * @swagger
   *
   * /news:
   *  post:
   *      tags:
   *          - News
   *      security:
   *          - JWT: [admin]
   *      summary: Add new news [admin]
   *      parameters:
   *          - name: News
   *            in: body
   *            required: true
   *            schema:
   *              $ref: "#/definitions/News"
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/News"
   *          500:
   *              description: Internal server error
   */
  async store({ request, response }) {
    const newsInfo = request.only(['title', 'details', 'contact', 'url']);
    const news = new News();
    news.title = newsInfo.title;
    news.details = newsInfo.details;
    news.contact = newsInfo.contact;
    news.url = newsInfo.url;

    try {
      await news.save();
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
    return news;
  }

  /**
   * @swagger
   *
   * /news/{id}:
   *  delete:
   *      tags:
   *          - News
   *      security:
   *          - JWT: [admin]
   *      summary: Delete news [admin]
   *      parameters:
   *          - name: id
   *            in: path
   *            required: true
   *      responses:
   *          200:
   *              description: Success
   *          404:
   *              description: Not found
   *          500:
   *              description: Internal server error
   */
  async destroy({ params, response }) {
    try {
      const news = await News.find(params.id);
      if (!news) {
        return response.status(404).send({ message: 'Resource not found.' });
      }
      await news.delete();
      return response.status(200).send({ message: 'Success.' });
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
  }
}

module.exports = NewsController;
