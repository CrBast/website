'use strict';
const Person = use('App/Models/Person');

class PersonController {
  /**
   * @swagger
   *
   * /persons:
   *  get:
   *      tags:
   *          - Persons
   *      summary: Get persons list
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/Person"
   *          500:
   *              description: Server error
   *          401:
   *              description: Unauthorized
   */
  async index() {
    var persons = await Person.query()
      .with('projects')
      .fetch();
    persons.rows.forEach(person => {
      person.skils = JSON.parse(person.skils);
    });
    return persons;
  }

  /**
   * @swagger
   *
   * /persons/{id}:
   *  get:
   *      tags:
   *          - Persons
   *      summary: Get specific person
   *      parameters:
   *          - name: id
   *            in: path
   *            required: true
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/Person"
   *          404:
   *              description: Not found
   *          401:
   *              description: Unauthorized
   */
  async show({ params, response }) {
    try {
      var person = await Person.findOrFail(params.id);
      await person.load('projects');
      person.skils = JSON.parse(person.skils);
      return person;
    } catch (error) {
      return response.status(404).send({ message: error.message });
    }
  }

  /**
   * @swagger
   *
   * /persons:
   *  post:
   *      tags:
   *          - Persons
   *      security:
   *          - JWT: [admin]
   *      summary: Add new person [admin]
   *      parameters:
   *          - name: Person
   *            in: body
   *            required: true
   *            schema:
   *              $ref: "#/definitions/Person"
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/Person"
   *          500:
   *              description: Internal server error
   */
  async store({ request, response }) {
    const personInfo = request.only([
      'firstname',
      'lastname',
      'email',
      'location',
      'skils'
    ]);
    const person = new Person();
    person.email = personInfo.email;
    person.firstname = personInfo.firstname;
    person.lastname = personInfo.lastname;
    person.location = personInfo.location;
    person.skils = JSON.stringify(personInfo.skils);

    try {
      await person.save();
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
    return person;
  }

  /**
   * @swagger
   *
   * /persons/{id}:
   *  put:
   *      tags:
   *          - Persons
   *      security:
   *          - JWT: [admin]
   *      summary: Full/Partial update [admin]
   *      parameters:
   *          - name: id
   *            in: path
   *            required: true
   *          - name: Person
   *            in: body
   *            required: true
   *            description: Full or partial Person object
   *            schema:
   *              $ref: "#/definitions/Person"
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/Person"
   *          404:
   *              description: Not found
   *          500:
   *              description: Internal server error
   */
  async update({ params, request, response }) {
    const personInfo = request.only([
      'firstname',
      'lastname',
      'email',
      'location',
      'skils'
    ]);
    const person = await Person.find(params.id);
    if (!person) {
      return response.status(404).json({ message: 'Resource not found.' });
    }
    person.firstname =
      personInfo.firstname == null ? person.firstname : personInfo.firstname;
    person.lastname =
      personInfo.lastname == null ? person.lastname : personInfo.lastname;
    person.email = personInfo.email == null ? person.email : personInfo.email;
    person.location =
      personInfo.location == null ? person.location : personInfo.location;
    person.skils =
      personInfo.skils == null
        ? person.skils
        : JSON.stringify(personInfo.skils);
    try {
      await person.save();
    } catch (error) {
      return response.status(500).json({ message: error.message });
    }
    return person;
  }

  /**
   * @swagger
   *
   * /persons/{id}:
   *  delete:
   *      tags:
   *          - Persons
   *      security:
   *          - JWT: [admin]
   *      summary: Delete Person [admin]
   *      parameters:
   *          - name: id
   *            in: path
   *            required: true
   *      responses:
   *          200:
   *              description: Success
   *          404:
   *              description: Not found
   *          500:
   *              description: Internal server error
   */
  async destroy({ params, response }) {
    try {
      const person = await Person.find(params.id);
      if (!person) {
        return response.status(404).send({ message: 'Resource not found.' });
      }
      await person.delete();
      return response.status(200).send({ message: 'Success.' });
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
  }
}

module.exports = PersonController;
