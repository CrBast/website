'use strict';
const User = use('App/Models/User');

class UserController {
  /**
   * @swagger
   *
   * /users:
   *  get:
   *      tags:
   *          - Users
   *      security:
   *          - JWT: []
   *      summary: Get users list
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/User"
   *          500:
   *              description: Server error
   *          401:
   *              description: Unauthorized
   */
  async index() {
    const user = await User.query()
      .with('companies')
      .with('jobOffers')
      .fetch();
    return user;
  }

  /**
   * @swagger
   *
   * /users/{id}:
   *  get:
   *      tags:
   *          - Users
   *      security:
   *          - JWT: []
   *      summary: Get specific user
   *      parameters:
   *          - name: id
   *            in: path
   *            required: true
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/User"
   *          404:
   *              description: Not found
   *          401:
   *              description: Unauthorized
   */
  async show({ params, response }) {
    try {
      const user = await User.findOrFail(params.id);
      await user.loadMany(['companies', 'jobOffers']);
      return user;
    } catch (error) {
      return response.status(404).send({ message: error.message });
    }
  }

  /**
   * @swagger
   *
   * /users:
   *  post:
   *      tags:
   *          - Users
   *      security:
   *          - JWT: []
   *      summary: Add new user [admin]
   *      parameters:
   *          - name: User
   *            in: body
   *            required: true
   *            schema:
   *              $ref: "#/definitions/User"
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/User"
   *          500:
   *              description: Internal server error
   *          401:
   *              description: Unauthorized
   */
  async store({ request, response }) {
    const userInfo = request.only(['email', 'password', 'role']);
    const user = new User();
    user.email = userInfo.email;
    user.password = userInfo.password;
    user.role = userInfo.role;
    try {
      await user.save();
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
    return user;
  }

  /**
   * @swagger
   *
   * /users/{id}:
   *  put:
   *      tags:
   *          - Users
   *      security:
   *          - JWT: []
   *      summary: Full/Partial update [admin]
   *      parameters:
   *          - name: id
   *            in: path
   *            required: true
   *          - name: user
   *            in: body
   *            required: true
   *            description: Full or partial user object
   *            schema:
   *              $ref: "#/definitions/User"
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/User"
   *          404:
   *              description: Not found
   *          500:
   *              description: Internal server error
   *          401:
   *              description: Unauthorized
   */
  async update({ params, request, response }) {
    const userInfo = request.only(['email', 'password', 'role']);
    const user = await User.find(params.id);
    if (!user) {
      return response.status(404).json({ message: 'Resource not found.' });
    }
    user.email = userInfo.email == null ? user.email : userInfo.email;
    user.password =
      userInfo.password == null ? user.password : userInfo.password;
    user.role = userInfo.role == null ? user.role : userInfo.role;
    try {
      await user.save();
    } catch (error) {
      return response.status(500).json({ message: error.message });
    }
    return user;
  }

  /**
   * @swagger
   *
   * /users/{id}:
   *  delete:
   *      tags:
   *          - Users
   *      security:
   *          - JWT: []
   *      summary: Delete user [admin]
   *      parameters:
   *          - name: id
   *            in: path
   *            required: true
   *      responses:
   *          200:
   *              description: Success
   *          404:
   *              description: Not found
   *          500:
   *              description: Internal server error
   *          401:
   *              description: Unauthorized
   */
  async destroy({ params, response }) {
    try {
      const user = await User.find(params.id);
      if (!user) {
        return response.status(404).send({ message: 'Resource not found.' });
      }
      await user.delete();
      return response.status(200).send({ message: 'Success.' });
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
  }
}

module.exports = UserController;
