'use strict';
const JobOffer = use('App/Models/JobOffer');
const Company = use('App/Models/Company');
const { boolConverter } = use('App/Helpers/BoolConvert');

class JobOfferController {
  /**
   * @swagger
   *
   * /job-offers:
   *  get:
   *      tags:
   *          - JobOffers
   *      summary: Get job offers list
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/JobOffer"
   *          500:
   *              description: Server error
   */
  async index() {
    var jobOffer = await JobOffer.all();
    jobOffer.rows.forEach(offer => {
      offer.isSerious = boolConverter(offer.isSerious);
    });
    return jobOffer.toJSON();
  }

  /**
   * @swagger
   *
   * /job-offers/{id}:
   *  get:
   *      tags:
   *          - JobOffers
   *      summary: Get specific job offer
   *      parameters:
   *          - name: id
   *            in: path
   *            required: true
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/JobOffer"
   *          404:
   *              description: Not found
   */
  async show({ params, response }) {
    try {
      var offer = await JobOffer.findOrFail(params.id);
      offer.isSerious = boolConverter(offer.isSerious);
      return offer;
    } catch (error) {
      return response.status(404).send({ message: error.message });
    }
  }

  /**
   * @swagger
   *
   * /job-offers:
   *  post:
   *      tags:
   *          - JobOffers
   *      security:
   *          - JWT: []
   *      summary: Add new job offer
   *      parameters:
   *          - name: JobOffer
   *            in: body
   *            required: true
   *            schema:
   *              $ref: "#/definitions/JobOffer"
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/JobOffer"
   *          500:
   *              description: Internal server error
   *          401:
   *              description: Unauthorized
   */
  async store({ request, response, auth }) {
    var jobOfferInfo = request.only([
      'company_id',
      'details',
      'url',
      'contact',
      'isSerious'
    ]);
    var userCompany = null;
    try {
      userCompany = await Company.findOrFail(jobOfferInfo.company_id);
    } catch (error) {
      return response.status(404).send({ message: error.message });
    }

    if (userCompany.user_id !== auth.user.id) {
      return response.status(401).send({
        message: "Only the company's founder can assign a job offer to him."
      });
    }

    try {
      jobOfferInfo.user_id = auth.user.id;
      const jobOffer = await userCompany.jobOffers().create(jobOfferInfo);
      return jobOffer;
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
  }

  /**
   * @swagger
   *
   * /job-offers/{id}:
   *  put:
   *      tags:
   *          - JobOffers
   *      security:
   *          - JWT: []
   *      summary: Full/Partial update [Only owner or admin]
   *      parameters:
   *          - name: id
   *            in: path
   *            required: true
   *          - name: jobOffer
   *            in: body
   *            required: true
   *            description: Full or partial jobOffer object
   *            schema:
   *              $ref: "#/definitions/JobOffer"
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/JobOffer"
   *          404:
   *              description: Not found
   *          500:
   *              description: Internal server error
   *          401:
   *              description: Unauthorized
   */
  async update({ params, request, response, auth }) {
    const jobOfferInfo = request.only([
      'company_id',
      'details',
      'url',
      'contact',
      'isSerious'
    ]);
    if (jobOfferInfo.company_id !== undefined) {
      var userCompany = null;
      try {
        userCompany = await Company.findOrFail(jobOfferInfo.company_id);
      } catch (error) {
        return response.status(404).send({ message: error.message });
      }

      if (userCompany.user_id !== (await auth.user.id)) {
        return response.status(401).send({
          message: "Only the company's founder can assign a job offer to him."
        });
      }
    }
    const jobOffer = await JobOffer.find(params.id);
    if (!jobOffer) {
      return response.status(404).json({ message: 'Resource not found.' });
    }
    jobOffer.company_id =
      jobOfferInfo.company_id == null
        ? jobOffer.company_id
        : jobOfferInfo.company_id;
    jobOffer.details =
      jobOfferInfo.details == null ? jobOffer.details : jobOfferInfo.details;
    jobOffer.url = jobOfferInfo.url == null ? jobOffer.url : jobOfferInfo.url;
    jobOffer.contact =
      jobOfferInfo.contact == null ? jobOffer.contact : jobOfferInfo.contact;
    jobOffer.isSerious =
      jobOfferInfo.isSerious == null
        ? boolConverter(jobOffer.isSerious)
        : boolConverter(jobOfferInfo.isSerious);
    try {
      await jobOffer.save();
    } catch (error) {
      return response.status(500).json({ message: error.message });
    }
    return jobOffer.toJSON();
  }

  /**
   * @swagger
   *
   * /job-offers/{id}:
   *  delete:
   *      tags:
   *          - JobOffers
   *      security:
   *          - JWT: []
   *      summary: Delete job offer [Only owner or admin]
   *      parameters:
   *          - name: id
   *            in: path
   *            required: true
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/JobOffer"
   *          404:
   *              description: Not found
   *          500:
   *              description: Internal server error
   *          401:
   *              description: Unauthorized
   */
  async destroy({ params, response }) {
    try {
      const jobOffer = await JobOffer.find(params.id);
      if (!jobOffer) {
        return response.status(404).send({ message: 'Resource not found.' });
      }
      await jobOffer.delete();
      return response.status(200).send({ message: 'Success.' });
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
  }
}

module.exports = JobOfferController;
