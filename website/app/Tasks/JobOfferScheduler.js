'use strict';
const Logger = use('Logger');

const Task = use('Task');
const JobOffer = use('App/Models/JobOffer');

class JobOfferScheduler extends Task {
  static get schedule() {
    return '0 2 * * *';
  }

  async handle() {
    Logger.transport('file').info(`Execute task : ${this.constructor.name}`);
    var tempJobs = await JobOffer.query()
      .where('isSerious', false)
      .delete();
    Logger.info(`Number of deleted job offers : ${tempJobs}`);
  }
}

module.exports = JobOfferScheduler;
