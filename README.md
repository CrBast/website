# crbast.ch

[![pipeline status](https://gitlab.com/CrBast/website/badges/develop/pipeline.svg)](https://gitlab.com/CrBast/website/commits/develop) 
[![coverage report](https://gitlab.com/CrBast/website/badges/develop/coverage.svg)](https://gitlab.com/CrBast/website/commits/develop)


## Open Source
- [AdonisJS](https://adonisjs.com/) - MVC framework (NodeJS)
- [adonis-swagger](https://github.com/ahmadarif/adonis-swagger) - Api documentation
- [HTML5 UP](https://html5up.net/) - Design

## Open API documentation
Swagger : `<host>`/docs

**[Wiki & Examples](https://gitlab.com/CrBast/website/wikis/home)**

## Analytics
I'm using [Shynet](https://github.com/milesmcc/shynet) for analytics. IP are not stored. The minimum data is used.
