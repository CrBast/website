# Install only production node modules
FROM node:lts-alpine as prod_packages
# Workaround
RUN apk add --no-cache git
# Set directory for all files
WORKDIR /home/node

COPY website/package.json ./
COPY website/yarn.lock ./

# Install only prod packages
RUN yarn install --production=true



# Build final runtime container
FROM node:lts-alpine
# Set environment variables
ENV NODE_ENV=production
ENV ENV_SILENT=true
ENV APP_KEY=

# Use non-root user
USER node

# Make directory for app to live in
# It's important to set user first or owner will be root
RUN mkdir -p /home/node/app/

WORKDIR /home/node/app

COPY website .

COPY --from=prod_packages /home/node/node_modules ./node_modules

EXPOSE 3333
CMD [ "node", "server.js" ]
